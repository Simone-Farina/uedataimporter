import re
import os
import pandas as pd

base_file_path = os.path.dirname(os.path.abspath(__file__))
big_ent_filename = os.path.join(base_file_path, "big_enterprises_output.csv")

big_ent_columns = ['permid', 'id', 'wave', 'disc', 'rejected', 'accepted2']
df = pd.read_csv(big_ent_filename, encoding='latin-1', dtype=str, usecols=big_ent_columns)

dct = {}
# check if permid has at least 1 disc
for index, row in df.iterrows():
    permid = row['permid']

    if permid in dct.keys():
        dct[permid].append(row['disc'])
    else:
        dct[permid] = [row['disc']]

cnt = 0
for permid in dct.keys():
    if '1' in dct[permid]:
        cnt += 1

print(cnt)

remove = []
for permid in dct.keys():
    if not '1' in dct[permid]:
        remove.append(permid)

for permid in reversed(remove):
    del dct[permid]

disc_dct = {}
for index, row in df.iterrows():
    permid = row['permid']

    if permid in dct.keys():
        if permid in disc_dct.keys():
            disc_dct[permid]['id'].append(row['id'])
            disc_dct[permid]['wave'].append(row['wave'])
            disc_dct[permid]['disc'].append(row['disc'])
            disc_dct[permid]['rejected'].append(row['rejected'])
            disc_dct[permid]['accepted2'].append(row['accepted2'])
        else:
            disc_dct[permid] = {'id': [row['id']], 'wave': [row['wave']], 'disc': [row['disc']],
                                'rejected': [row['rejected']], 'accepted2': [row['accepted2']]}

out_dct = {}
for permid in disc_dct.keys():
    disc_count = len([d for d in disc_dct[permid]['disc'] if d == '1'])
    disc_idx = disc_dct[permid]['disc'].index('1')

    if disc_count == 0:
        out_dct[permid] = disc_dct[permid]
    elif len(disc_dct[permid]['disc']) == disc_count:
        out_dct[permid] = disc_dct[permid]
    elif disc_idx == 0 and disc_count == 1:
        out_dct[permid] = disc_dct[permid]

for permid in out_dct.keys():
    del disc_dct[permid]

for permid in disc_dct.keys():
    temp_cnt = 0
    disc_dct[permid]['progr'] = []
    for wave in disc_dct[permid]['wave']:
        if temp_cnt == 0:
            disc_dct[permid]['progr'].append(0)
        else:
            if int(wave.strip('id')) == int(disc_dct[permid]['wave'][temp_cnt-1].strip('id')) + 1:
                disc_dct[permid]['progr'].append(disc_dct[permid]['progr'][temp_cnt-1])
            else:
                disc_dct[permid]['progr'].append(disc_dct[permid]['progr'][temp_cnt - 1] + 1)
        temp_cnt += 1

out_dct = {}
tmp = {}
for permid in disc_dct.keys():
    max_progr = disc_dct[permid]['progr'][-1]
    for i in range(max_progr+1):
        lower_bound = disc_dct[permid]['progr'].index(i)
        upper_bound = disc_dct[permid]['progr'].index(i+1) - 1 if i+1 in disc_dct[permid]['progr'] else \
            len(disc_dct[permid]['progr']) - 1

        temp_disc = disc_dct[permid]['disc'][lower_bound:upper_bound+1]
        disc_count = len([d for d in temp_disc if d == '1'])

        if disc_count == 0:
            pass
        elif disc_count == 1 and temp_disc[0] == '1':
            pass
        elif disc_count == len(temp_disc):
            pass
        elif temp_disc == ['0', '1']:
            if permid in out_dct.keys():
                out_dct[permid]['id'] += disc_dct[permid]['id'][lower_bound:upper_bound+1]
                out_dct[permid]['wave'] += disc_dct[permid]['wave'][lower_bound:upper_bound+1]
                out_dct[permid]['rejected'] += disc_dct[permid]['rejected'][lower_bound:upper_bound+1]
                out_dct[permid]['accepted2'] += disc_dct[permid]['accepted2'][lower_bound:upper_bound+1]
                out_dct[permid]['disc'] += temp_disc
            else:
                out_dct[permid] = {'id': disc_dct[permid]['id'][lower_bound:upper_bound+1],
                                   'wave': disc_dct[permid]['wave'][lower_bound:upper_bound+1],
                                   'rejected': disc_dct[permid]['rejected'][lower_bound:upper_bound+1],
                                   'accepted2': disc_dct[permid]['accepted2'][lower_bound:upper_bound+1],
                                   'disc': temp_disc}
        else:
            temp_str = ''.join(str(e) for e in temp_disc)
            for m in re.finditer('01', temp_str):
                start_idx = m.start()
                if m.start() > 0 and temp_str[m.start() - 1] == '0':
                    start_idx = m.start() - 1

                if permid in out_dct.keys():
                    out_dct[permid]['id'] += disc_dct[permid]['id'][start_idx:m.end()]
                    out_dct[permid]['wave'] += disc_dct[permid]['wave'][start_idx:m.end()]
                    out_dct[permid]['rejected'] += disc_dct[permid]['rejected'][start_idx:m.end()]
                    out_dct[permid]['accepted2'] += disc_dct[permid]['accepted2'][start_idx:m.end()]
                    out_dct[permid]['disc'] += temp_disc[start_idx:m.end()]
                else:
                    out_dct[permid] = {'id': disc_dct[permid]['id'][start_idx:m.end()],
                                       'wave': disc_dct[permid]['wave'][start_idx:m.end()],
                                       'disc': temp_disc[start_idx:m.end()],
                                       'rejected': disc_dct[permid]['rejected'][start_idx:m.end()],
                                       'accepted2': disc_dct[permid]['accepted2'][start_idx:m.end()]}

disc_out = {
    'permid': [],
    'id': [],
    'wave': [],
    'disc': [],
    'rejected': [],
    'accepted2': []
}

# transpose dictionary to pd_table
for permid in out_dct.keys():
    for idx, _ in enumerate(out_dct[permid]['id']):
        # if '1' in out_dct[permid]['accepted2'] and (out_dct[permid]['accepted2'][idx] == '1'
        #                                           or out_dct[permid]['disc'][idx] == '1'):
        if '1' in out_dct[permid]['rejected'] and (out_dct[permid]['rejected'][idx] == '1'
                                                   or out_dct[permid]['disc'][idx] == '1'):
            disc_out['permid'].append(permid)
            disc_out['id'].append(out_dct[permid]['id'][idx])
            disc_out['wave'].append(out_dct[permid]['wave'][idx])
            disc_out['disc'].append(out_dct[permid]['disc'][idx])
            disc_out['rejected'].append(out_dct[permid]['rejected'][idx])
            disc_out['accepted2'].append(out_dct[permid]['accepted2'][idx])

df = pd.DataFrame.from_dict(disc_out)
output_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'disc_output.csv')
df.to_csv(output_filename, index=False, sep=',', mode='w')
