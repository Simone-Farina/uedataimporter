# sed -i '' 's+;+,+g' panel_keytable.csv

import os
import pandas as pd

col_avg = 'q0b_3'

START_WAVE = 1
STOP_WAVE = 23

base_file_path = os.path.dirname(os.path.abspath(__file__))
panel_filename = os.path.join(base_file_path, "panel_keytable.csv")
safe_filename = os.path.join(base_file_path, "safe_allrounds.csv")

safe_col_list = ['id', 'd1_rec', 'q4_d', 'q7A_a', 'q7B_a', 'q7A_b', 'q7A_c', 'q7A_d', 'q17']
df_panel = pd.read_csv(panel_filename, encoding='latin-1', dtype=str)
df_panel = df_panel.fillna('Invalid')
df_safe = pd.read_csv(safe_filename, encoding='latin-1', dtype=str, usecols=safe_col_list)
df_safe = df_safe.fillna('Invalid')


def find_in_sorted_column(element, series, low, high):
    if high >= low:
        mid = (high + low) // 2

        if series[mid] == element:
            return mid

        elif series[mid] > element:
            return find_in_sorted_column(element, series, low, mid - 1)

        else:
            return find_in_sorted_column(element, series, mid + 1, high)

    else:
        return -1


sorted_safe_ids = list(map(int, list(df_safe['id'])))


def check_adjacent_cell(row_idx, column_name, allow_empty=1, dataframe=df_panel) -> bool:
    l_found = False
    r_found = False
    discard_lower_bound = START_WAVE + 1
    discard_upper_bound = STOP_WAVE + 2

    # range (1, 3) -> 1, 2
    for i in range(1, allow_empty + 2):
        current_idx = dataframe.columns.get_loc(column_name)

        if current_idx - i > discard_lower_bound:
            l_x = dataframe.iloc[row_idx, current_idx - i]
            if l_x != 'Invalid':
                l_found = True

        if current_idx + i < discard_upper_bound:
            r_x = dataframe.iloc[row_idx, current_idx + i]
            if r_x != 'Invalid':
                r_found = True

    return l_found or r_found


def check_value_eq(ref: str, value: int, value1: int = None, ret: int = 1, inv_ret=''):
    if ref == 'Invalid':
        return inv_ret
    else:
        if int(ref) == value or int(ref) == value1:
            return ret
        else:
            return 0


out_columns = ['permid', 'id', 'wave', 'disc', 'rejected', 'accepted1',
               'accepted2', 'altre_fonti', 'growth_pos', 'growth_stab', 'growth_neg']

out_ril_dict = {k: [] for k in out_columns}

# extracting relevant permids
cnt = 0
for index, row in df_panel.iterrows():
    cnt += 1
    print(cnt)
    count = START_WAVE

    while count != STOP_WAVE + 1:
        id = 'id' + str(count)

        if row[id] != 'Invalid' and check_adjacent_cell(index, id, allow_empty=0):
            out_ril_dict['permid'].append(row['permid'])
            out_ril_dict['id'].append(row[id])
            out_ril_dict['wave'].append(id)

            idx = find_in_sorted_column(int(row[id]), sorted_safe_ids, 0, len(sorted_safe_ids))
            dct = df_safe.iloc[idx].to_dict()

            # check if relevant
            if int(dct['q4_d']) == 3:
                out_ril_dict['rejected'].append(check_value_eq(dct['q7B_a'], 4, inv_ret='0'))
                out_ril_dict['accepted1'].append(check_value_eq(dct['q7B_a'], 1, 5, inv_ret='0'))
                out_ril_dict['accepted2'].append(check_value_eq(dct['q7B_a'], 6, inv_ret='0'))

                if (dct['q7A_b'] != 'Invalid' and int(dct['q7A_b']) == 3) or \
                        (dct['q7A_c'] != 'Invalid' and int(dct['q7A_c']) == 3) or \
                        (dct['q7A_d'] != 'Invalid' and int(dct['q7A_d']) == 3):
                    out_ril_dict['altre_fonti'].append(1)
                else:
                    out_ril_dict['altre_fonti'].append(0)

                out_ril_dict['growth_pos'].append(check_value_eq(dct['q17'], 1, 2))
                out_ril_dict['growth_stab'].append(check_value_eq(dct['q17'], 3))
                out_ril_dict['growth_neg'].append(check_value_eq(dct['q17'], 4))

                if int(dct['q7A_a']) == 2:
                    out_ril_dict['disc'].append(1)
                elif int(dct['q7A_a']) == 1:
                    out_ril_dict['disc'].append(0)
                else:
                    for k in out_ril_dict.keys():
                        if k != 'disc':
                            del out_ril_dict[k][-1]

            else:
                del out_ril_dict['permid'][-1]
                del out_ril_dict['id'][-1]
                del out_ril_dict['wave'][-1]

        count += 1

df = pd.DataFrame.from_dict(out_ril_dict)
output_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'temp.csv')
df.to_csv(output_filename, index=False, sep=',', mode='w')

ALLOW_EMPTY = 1
count = 0

delete_lst_indexes = []
for _ in out_ril_dict['permid']:
    print(count)
    current_permid = out_ril_dict['permid'][count]
    adjacency_found = False

    # count = 1: (2, 3) -> [2]
    # count = 0, allow_empty = 1: (1, 3) -> [1, 2]
    cnt = 1
    for idx in range(count + 1, count + ALLOW_EMPTY + 2):
        if idx <= len(out_ril_dict['permid']) - 1:
            # forward check
            if current_permid == out_ril_dict['permid'][count + 1]:
                if int(out_ril_dict['wave'][count].strip('id')) == int(out_ril_dict['wave'][count + 1].strip('id')) - cnt:
                    adjacency_found = True
                    break
                # backward check
                elif count > 0 and current_permid == out_ril_dict['permid'][count - 1]:
                    if int(out_ril_dict['wave'][count].strip('id')) == int(out_ril_dict['wave'][count - 1].strip('id')) + cnt:
                        adjacency_found = True
                        break

            elif count > 0 and current_permid == out_ril_dict['permid'][count - 1]:
                if int(out_ril_dict['wave'][count].strip('id')) == int(out_ril_dict['wave'][count - 1].strip('id')) + cnt:
                    adjacency_found = True
                    break

            cnt += 1

    # do not work on iterating collection (while reverse iteration or copy())
    if not adjacency_found:
        delete_lst_indexes.append(count)

    count += 1

for idx in reversed(delete_lst_indexes):
    for k in out_ril_dict.keys():
        del out_ril_dict[k][idx]

df = pd.DataFrame.from_dict(out_ril_dict)
output_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ril_output_bck.csv')
df.to_csv(output_filename, index=False, sep=',', mode='w')
