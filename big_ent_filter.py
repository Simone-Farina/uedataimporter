# this script MUST be executed after running main2.py that generates ril_output_bck.csv
import copy
import os
import pandas as pd

base_file_path = os.path.dirname(os.path.abspath(__file__))
ril_filename = os.path.join(base_file_path, "ril_output_bck.csv")
safe_filename = os.path.join(base_file_path, "safe_allrounds.csv")

safe_col_list = ['id', 'd1_rec', 'd3_rec', 'd5_rec', 'q2_a', 'q2_j', 'q5_a', 'q11_d', 'q0b_3', 'q11_e', 'q11_f']
df_safe = pd.read_csv(safe_filename, encoding='latin-1', dtype=str, usecols=safe_col_list)
df_safe = df_safe.fillna('Invalid')
df_ril = pd.read_csv(ril_filename, encoding='latin-1', dtype=str)
df_ril = df_ril.fillna('Invalid')

sorted_safe_ids = list(map(int, list(df_safe['id'])))


def find_in_sorted_column(element, series, low, high):
    if high >= low:
        mid = (high + low) // 2

        if series[mid] == element:
            return mid

        elif series[mid] > element:
            return find_in_sorted_column(element, series, low, mid - 1)

        else:
            return find_in_sorted_column(element, series, mid + 1, high)

    else:
        return -1


def check_value_eq(ref: str, value: int, value1: int = None, ret: int = 1):
    print(ref)
    if ref == 'Invalid':
        return None
    else:
        if int(ref) == value or int(ref) == value1:
            return ret
        else:
            return None


out_columns = {'SIZE': 'd1_rec',
               'AGE': 'd5_rec',
               'TURNOVER': 'q2_a',
               'FINSLACK': 'q2_j',
               'NEEDS': 'q5_a',
               'CAPITAL': 'q11_d',
               'ACCESS': 'q0b_3',
               'CREDITHISTORY': 'q11_e',
               'WILLING': 'q11_f'}

out_dict = {k: list() for k in df_ril.columns}
out_dict.update({k: list() for k in out_columns})
for index, row in df_ril.iterrows():
    print(index)
    id_ = row['id']

    safe_idx = find_in_sorted_column(int(id_), sorted_safe_ids, 0, len(sorted_safe_ids))
    dct = df_safe.iloc[safe_idx].to_dict()

    if int(dct['SIZE']) == 4 or int(dct['SIZE']) == 9:
        pass
    else:
        for k in out_dict.keys():
            if k not in out_columns.keys():
                out_dict[k].append(row[k])
            else:
                if k == 'ACCESS':
                    if int(dct[out_columns[k]]) == 10:
                        out_dict[k].append(1)
                    elif int(dct[out_columns[k]]) in range(1, 10, 1) or int(dct[out_columns[k]]) == 99:
                        out_dict[k].append(0)
                    else:
                        out_dict[k].append('')
                else:
                    out_dict[k].append(dct[out_columns[k]])

df = pd.DataFrame.from_dict(out_dict)
dct = {}

ALLOW_EMPTY = 0
count = 0

delete_lst_indexes = []
for _ in out_dict['permid']:
    print(count)
    current_permid = out_dict['permid'][count]
    adjacency_found = False

    # count = 1: (2, 3) -> [2]
    # count = 0, allow_empty = 1: (1, 3) -> [1, 2]
    cnt = 1
    for idx in range(count + 1, count + ALLOW_EMPTY + 2):
        if idx <= len(out_dict['permid']) - 1:
            # forward check
            if current_permid == out_dict['permid'][count + 1]:
                if int(out_dict['wave'][count].strip('id')) == int(out_dict['wave'][count + 1].strip('id')) - cnt:
                    adjacency_found = True
                    break
                # backward check
                elif count > 0 and current_permid == out_dict['permid'][count - 1]:
                    if int(out_dict['wave'][count].strip('id')) == int(out_dict['wave'][count - 1].strip('id')) + cnt:
                        adjacency_found = True
                        break

            elif count > 0 and current_permid == out_dict['permid'][count - 1]:
                if int(out_dict['wave'][count].strip('id')) == int(out_dict['wave'][count - 1].strip('id')) + cnt:
                    adjacency_found = True
                    break

            cnt += 1

    # do not work on iterating collection (while reverse iteration or copy())
    if not adjacency_found:
        delete_lst_indexes.append(count)

    count += 1

for idx in reversed(delete_lst_indexes):
    for k in out_dict.keys():
        del out_dict[k][idx]

df = pd.DataFrame.from_dict(out_dict)
output_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'big_enterprises_output.csv')
df.to_csv(output_filename, index=False, sep=',', mode='w')
